{$h1}Sudoku{$_h1}

{$p}<b>Instrucciones</b>: Rellene las celdas vac&iacute;as con un n&uacute;mero del <b>1 al 9</b> de tal forma <b>que no se repitan</b> en cada columna, fila y cuadrante. Los cuadrantes est&aacute;n delimitados por los borders negros. Despl&aacute;cese hacia abajo para ver la soluci&oacute;n{$_p}

{button service="SUDOKU"}Otro Sudoku!{$_button}
{link service="ARTICULO sudoku"}Aprender m&aacute;s sobre el juego{$_link}
{$br}
  
{$h1}Completar desde su correo{$_h1}
<!--[if mso]>
	{$p}Usted est&aacute; usando una versi&oacute;n posterior a Outlook 2007, por lo que posiblemente no pueda responder el sudoku en esta ventana. Si observa arriba hay un enlace de Outlook que empieza diciendo <i>"Si hay problemas con el modo en que se muestra este mensaje,..."</i>. Haga clic en ese mensaje para abrir el sudoku en un navegador web.{$_p}
<![endif]-->
{$problem}
{$br}{$br}

{$h1}Imprimir{$_h1}
{$problem_print}
{$br}{$br}

{$h1}Soluci&oacute;n{$_h1}
{$solution}